package ru.javarush.todolist.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javarush.todolist.entity.Task;

import java.util.List;

/**
 * Created by Aleksandr on 22-Nov-16.
 */
public interface TaskRepository extends CrudRepository<Task, Long> {
    List<Task> findByCompletedTrue();
}
