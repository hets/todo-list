package ru.javarush.todolist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.javarush.todolist.entity.Task;
import ru.javarush.todolist.repository.TaskRepository;

import java.util.List;

/**
 * Created by Aleksandr on 16-Nov-16.
 */
@RestController
@RequestMapping("/api")
public class TodoController {
    @Autowired
    private TaskRepository taskRepository;

    @RequestMapping
    public void api() {
    }

    @RequestMapping(path = "/todos", method = RequestMethod.GET)
    public Iterable<Task> get() {
        return taskRepository.findAll();
    }

    @RequestMapping(path = "/todos", method = RequestMethod.POST)
    public Task post(@RequestBody Task task) {
        return taskRepository.save(task);
    }

    @RequestMapping(path = "/todos/{id}", method = RequestMethod.PUT)
    public void put(@PathVariable("id") Long id, @RequestBody Task task) {
        taskRepository.save(task);
    }

    @RequestMapping(path = "/todos/{id}", method = RequestMethod.DELETE)
    public void deleteTask(@PathVariable("id") Long id) {
        taskRepository.delete(id);
    }

    @RequestMapping(path = "/todos", method = RequestMethod.DELETE)
    public void deleteCompleted() {
        List<Task> completedTasks = taskRepository.findByCompletedTrue();
        taskRepository.delete(completedTasks);
    }
}
